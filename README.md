# Code Challenge
The Code Challenge UI is developed with a react redux approach along with react router to manage redirection to various portions of the web app as a single page app implementation.

## Running Locally
- ```yarn install``` or ```npm run install```
- ```yarn start``` or ```npm run start```
- Auth state redirection currently isn't included. However, without a proper auth state the web app won't work.
  - Have the Java API service running
  - In a browser navigate to ```http://localhost:/v1/api/login```
  - You will then be redirected to an auth0 hosted experience where you will need to sign up
  - Once signed up, auth0 will redirect back to a route exposed by the web app and pass the access token to it
- You can now use the app to 
  - Search for GIFS
  - Save GIFS
  - View the saved GIFS
  - Create categories
  - And recategorize your saved GIFS

## Notes
This web app is only for testing purposes. It should in no way be considered a production ready app.