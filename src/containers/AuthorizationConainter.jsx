import { connect } from 'react-redux';
import Authorization from '../components/Authorization';
import { updateAuthentication, saveAccessToken } from '../actions/auth-actions';


const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
});
  
const mapDispatchToProps = dispatch => ({
  updateAuthentication: isAuthenticated => dispatch(updateAuthentication(isAuthenticated)),
  saveAccessToken : accessToken => dispatch(saveAccessToken(accessToken)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Authorization);