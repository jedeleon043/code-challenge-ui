import { connect } from 'react-redux';
import SavedGifs from '../components/SavedGifs';

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    accessToken: state.auth.accessToken,
    gifs: state.giphy.giphyData
  };
};

  
export default connect(mapStateToProps)(SavedGifs);