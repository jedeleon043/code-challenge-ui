import { connect } from 'react-redux';
import GiphyModal from '../components/GiphyModal';
import {storeGiphyResponse} from '../actions/giphy-actions';

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    accessToken: state.auth.accessToken
  };
};


const mapDispatchToProps = dispatch => ({
  storeGiphyResponse: data => dispatch(storeGiphyResponse(data)),
});
  
export default connect(mapStateToProps, mapDispatchToProps)(GiphyModal);