import { connect } from 'react-redux';
import Gifs from '../components/Gifs';

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    accessToken: state.auth.accessToken,
    gifs: state.giphy.giphyData
  };
};

  
export default connect(mapStateToProps)(Gifs);