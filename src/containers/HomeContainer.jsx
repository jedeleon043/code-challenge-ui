

import { connect } from 'react-redux';
import Home from '../components/Home';

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    accessToken: state.auth.accessToken
  };
};
  
export default connect(mapStateToProps)(Home);