import { connect } from 'react-redux';
import Header from '../components/Header';

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    accessToken: state.auth.accessToken
  };
};
  
export default connect(mapStateToProps)(Header);