import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import getReducers from './reducers/reducers';
import promisMiddleware from 'redux-promise-middleware';
import thunk from 'redux-thunk';
import {routerMiddleware} from 'react-router-redux';

export default function configureStore(history, initiaState) {
    let reducer = combineReducers(getReducers());

    let middleware = [thunk, routerMiddleware(history), promisMiddleware()];

    let enhancements = [applyMiddleware(...middleware)];

    if (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__) {
        enhancements.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }

    return createStore(reducer, initiaState, compose(...enhancements));
}