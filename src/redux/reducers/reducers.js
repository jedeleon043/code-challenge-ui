
import authenticationReducer from './auth-reducer';
import giphyReducer from './giphy-reducer';

export default function getReducers() {
    return {
        auth: authenticationReducer,
        giphy: giphyReducer,
    }
}