import {
    AUTHENTICATION, ACCESS_TOKEN
  } from '../../actions/auth-actions';
  
  let initialState = {
    isAuthenticated: false,
    accessToken: ''
  };
  
  export default function authenticationReducer (state = initialState, action = '') {
    switch (action.type) {
      case `${AUTHENTICATION}_REJECTED` :
        return {
          ...state,
          errorMessage: 'Error with authentication',
        };
      case `${AUTHENTICATION}` : {
        return {
          ...state,
          ...action.payload,
          errorMessage: '',
        };
      }
      case `${ACCESS_TOKEN}`: {
        return {
          ...state,
          ...action.payload
        }
      }
      default: 
        return state;
    }
  }