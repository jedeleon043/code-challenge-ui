import {
    GIPHY
  } from '../../actions/giphy-actions';
  
  let initialState = {
      giphyData: {}
  };
  
  export default function giphyReducer (state = initialState, action = '') {
    switch (action.type) {
      case `${GIPHY}_REJECTED` :
        return {
          ...state,
          errorMessage: 'Error with authentication',
        };
      case `${GIPHY}` : {
        return {
          ...state,
          ...action.payload,
        };
      }
      default: 
        return state;
    }
  }