import React, { Component } from 'react';
import './GiphyModal.css';
import {Modal, FormGroup, FormControl} from 'react-bootstrap';
import Button from 'react-bootstrap-button-loader';
import {withRouter} from "react-router-dom";

class GiphyModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            query: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit = event => {
        this.setState({loading: true});

        const tokenType = this.props.accessToken['token_type'];
        const accessToken = this.props.accessToken['access_token'];
        const options = {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'content-type': 'application/json',
                'Authorization': tokenType + " " + accessToken
            }
        }
        fetch('http://localhost:8080/v1/api/giphy/search?query=' + this.state.query, options)
        .then(response => response.json())
        .then(json => {
            this.setState({loading: false});
            this.props.storeGiphyResponse(json);
            this.props.history.push('/gifs');
            this.props.handleModalClose();
        })
        .catch(e => {
            console.log(e);
        })


    }

    handleChange = event => {
        console.log(event);
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    render() {
        return (
            <div>
                <Modal.Dialog>
                    <Modal.Header>
                        <Modal.Title> Search GIPHY!</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <FormGroup controlId="query">
                                <FormControl 
                                    autoFocus
                                    type="text" 
                                    placeholder="Search" 
                                    value={this.state.query}
                                    onChange={this.handleChange}
                                />
                            </FormGroup>
                            <Button 
                                loading={this.state.loading} 
                                onClick={this.handleSubmit} 
                                type="submit"
                                spinColor="#000000">
                                    Submit
                                </Button>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.props.handleModalClose}>Close</Button>
                    </Modal.Footer>
                </Modal.Dialog>
            </div>
        );
    }
};

export default withRouter(GiphyModal);
