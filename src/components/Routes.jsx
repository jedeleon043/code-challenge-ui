import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AuthorizatonContainer  from '../containers/AuthorizationConainter'
import HomeContainer from '../containers/HomeContainer';
import GifsContainer from '../containers/GifsContainer';
import SavedGifsContainer from '../containers/SavedGifsContainer';

export default (props) => (
  <div>
    <Switch>
      <Route path="/authorize" exact component={AuthorizatonContainer} />
      <Route path="/" exact component={HomeContainer} />
      <Route path="/gifs" exact component={GifsContainer} />
      <Route path="/saved-gifs" exact component={SavedGifsContainer} />
    </Switch>
  </div>
);