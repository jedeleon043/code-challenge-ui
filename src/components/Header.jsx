import React, { Component } from 'react';
import './Header.css';
import {Navbar, Nav, NavItem, Button} from 'react-bootstrap';
import GiphyModalContainer from '../containers/GiphyModalContainer';
import {withRouter} from "react-router-dom";
class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }

        this.handleSelect = this.handleSelect.bind(this);
        this.handleModalClose = this.handleModalClose.bind(this);
        this.handleHomeClick = this.handleHomeClick.bind(this);
    }

    handleSelect = selected => {
        switch (selected){
            case 1:
                // TODO navigate to categorized gifs
                this.props.history.push('/saved-gifs');
                break;
            case 2:
                this.setState({showModal: true});
                break;
            default:
                break;
        }

    }

    handleHomeClick = () => {
        this.props.history.push('/');
    }

    handleModalClose = () => {
        this.setState({showModal: false});
    }

    render() {
        return (
            <div id="header">
                <Navbar inverse collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Button bsClass="homebutton" onClick={this.handleHomeClick}>
                            The GIFS App
                        </Button>
                        {/* <a href="#home"> The GIFS App </a> */}
                    </Navbar.Brand>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                <Nav onSelect={this.handleSelect}>
                    <NavItem eventKey={1} href="#">
                        Saved Gifs
                    </NavItem>
                    <NavItem eventKey={2} href="#">
                        Search GIPHY
                    </NavItem>
                    {/* <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
                        <MenuItem eventKey={3.1}>Action</MenuItem>
                        <MenuItem eventKey={3.2}>Another action</MenuItem>
                        <MenuItem eventKey={3.3}>Something else here</MenuItem>
                        <MenuItem divider />
                        <MenuItem eventKey={3.3}>Separated link</MenuItem>
                    </NavDropdown> */}
                </Nav>
                <Nav pullRight>
                    {/* <Navbar.Form pullRight>
                        <FormGroup>
                            <FormControl type="text" placeholder="Search" />
                        </FormGroup>
                        <Button type="submit">Submit</Button>
                    </Navbar.Form> */}
                </Nav>
                </Navbar.Collapse>
                </Navbar>
                {(this.state.showModal)?<GiphyModalContainer handleModalClose={this.handleModalClose}/>:null}
            </div>
        );
    }
};
export default withRouter(Header);