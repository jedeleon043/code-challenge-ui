import React, { Component } from 'react';

export default class Authorization extends Component {

    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    componentDidMount() {
        let authData = {};
        let authContents = window.location.hash;
        authContents = authContents.substring(1, authContents.length);
        authContents.split('&').map(key => {
            const param = key.split('=');
            authData[param[0]] = param[1];
        });
        console.log(authData);
        this.props.updateAuthentication(true);
        this.props.saveAccessToken(authData);
    }

    render() {
        return (
            <div id="authorization">
                HOME
            </div>
        );
    }
};
