import React, { Component } from 'react';
import './GiphyModal.css';
import {Button, ButtonToolbar, ToggleButton, ToggleButtonGroup, Image} from 'react-bootstrap';
import {withRouter} from "react-router-dom";

class Gifs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected:[],
            maxColumns: 3,
            submitting: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    handleChange = selectedGif => {
        console.log(selectedGif);
        this.setState({selected: selectedGif});
    }

    handleSubmit = () => {
        this.setState({submitting: true});

        const tokenType = this.props.accessToken['token_type'];
        const accessToken = this.props.accessToken['access_token'];
        const options = {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'content-type': 'application/json',
                'Authorization': tokenType + " " + accessToken
            },
            body: JSON.stringify({
                giphyIds: this.state.selected
            })
        }
        fetch('http://localhost:8080/v1/api/gif', options)
        .then(response =>{
            this.setState({submitting: false});
            this.props.history.push('/saved-gifs');
        })
        .catch(e => {
            console.log(e);
        })


    }

    render() {
        let rows = [];
        return (
            <ButtonToolbar>
                <Button bsStyle="save" 
                    onClick={this.handleSubmit}
                    loading={this.state.loading} 
                    spinColor="#000000"
                    >Save</Button>
                <br/>
                <br/>
                {this.props.gifs.data.map((gif, index) => {
                    let columnIndex = index % this.state.maxColumns;
                    let row = (columnIndex === 0)? {col0: '', col1: '', col2: ''}: rows.pop();

                    row['col'+columnIndex] = {
                        id: gif.id,
                        url: gif.images.downsized.url
                    };

                    rows.push(row);

                })}
                {
                    rows.map((row, index) => {
                        return(
                            <ToggleButtonGroup
                            type="checkbox"
                            value={this.state.selected}
                            onChange={this.handleChange}>
                            
                                <ToggleButton value={row.col0.url}>
                                    <Image id={row.col0.id} src={row.col0.url} rounded responsive/> 
                                </ToggleButton>
                                <ToggleButton  value={row.col0.url}>
                                    <Image id={row.col1.id} src={row.col1.url} rounded responsive/>
                                </ToggleButton>
                                <ToggleButton value={row.col0.url}>
                                    <Image id={row.col2.id} src={row.col2.url} rounded responsive/>
                                </ToggleButton>
                            </ToggleButtonGroup>
                        );
                    })
                }
            </ButtonToolbar>
        );
    }
};
export default withRouter(Gifs);