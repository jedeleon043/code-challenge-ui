import React, { Component } from 'react';
import './GiphyModal.css';
import {Form, ListGroup, ListGroupItem, Panel, Image, FormGroup, ControlLabel, FormControl, Button, SplitButton, MenuItem} from 'react-bootstrap';
import {withRouter} from "react-router-dom";

class SavedGifs extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            gifs: [],
            categoryName: '',
            categoryAdding: false,
            categories: [],
            giphyMap:{}
        }
        this.getSavedGifs = this.getSavedGifs.bind(this);
        this.handleNewCategory = this.handleNewCategory.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
    }

    componentDidMount() {
        this.getSavedGifs();
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    handleNewCategory = () => {
        this.setState({categoryAdding: true});
        const tokenType = this.props.accessToken['token_type'];
        const accessToken = this.props.accessToken['access_token'];
        const options = {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'content-type': 'application/json',
                'Authorization': tokenType + " " + accessToken
            },
            body: JSON.stringify({
                categoryName: this.state.categoryName
            })
        }
        fetch('http://localhost:8080/v1/api/categories', options)
        .then(response => {
            this.setState({categoryAdding: false, categories: [...this.state.categories, this.state.categoryName], categoryName: ''});
        })
        .catch(e => {
            console.log(e);
        })
    }

    handleCategoryChange = (event) => {
        console.log(event.target.id);
        console.log(event.target.value);


        this.setState({categoryAdding: true});
        const tokenType = this.props.accessToken['token_type'];
        const accessToken = this.props.accessToken['access_token'];
        const options = {
            method: 'PUT',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'content-type': 'application/json',
                'Authorization': tokenType + " " + accessToken
            },
            body: JSON.stringify({
                currentCategoryName: this.state.giphyMap[event.target.id],
                categoryName: event.target.value
            })
        }
        fetch('http://localhost:8080/v1/api/gif?id='+event.target.id, options)
        .then(response => {
            let updateGiphyMap = this.state.giphyMap;
            updateGiphyMap[event.target.id] = event.target.value;
            this.setState({
                categoryAdding: false, 
                categories: [...this.state.categories, this.state.categoryName], 
                categoryName: '',
                giphyMap: updateGiphyMap
            });
        })
        .catch(e => {
            console.log(e);
        })

    }

    getSavedGifs = () => {
        this.setState({loading: true});

        const tokenType = this.props.accessToken['token_type'];
        const accessToken = this.props.accessToken['access_token'];
        const options = {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'content-type': 'application/json',
                'Authorization': tokenType + " " + accessToken
            }
        }
        fetch('http://localhost:8080/v1/api/categories', options)
        .then(response => response.json())
        .then(json => {
            let categories = json.map(gif => {return gif.categoryName});
            let giphyMapItem = json.map(gif => {
                let giphyIds = gif.gifObjects.map(gifObj => gifObj.giphyId);
                return {categoryName: gif.categoryName, giphyIds };
            });
            let giphyMapper = {};
            for(let giphyItem in giphyMapItem) {
                let giphyIds = giphyMapItem[giphyItem].giphyIds
                for(let giphyId in giphyIds) {
                    giphyMapper[giphyIds[giphyId]] = giphyMapItem[giphyItem].categoryName;
                }
            }
            this.setState({gifs: json, loading: false, categories, giphyMap: giphyMapper});
        })
        .catch(e => {
            console.log(e);
        })


    }

    render() {
        let gifRender = (!this.state.loading && this.state.gifs.length > 0)? this.state.gifs.map((gif, index) => {
            return(
                <Panel key={gif.categoryName}>
                        <Panel.Heading><h1>{gif.categoryName}</h1></Panel.Heading>
                        <Panel.Body>
                            <ListGroup>
                                {gif.gifObjects.map((gifObject, index) =>{
                                    return(
                                        <ListGroupItem key={gifObject.giphyId}>
                                            <Image src={gifObject.giphyId} responsive/>

                                                <FormGroup controlId={`${gifObject.giphyId}`}>
                                                    <ControlLabel>Category Selector</ControlLabel>
                                                    <FormControl 
                                                        componentClass="select" 
                                                        placeholder="Change Category"
                                                        onChange={this.handleCategoryChange}
                                                        >
                                                        {this.state.categories.map((category, index) => {
                                                            return(
                                                                    <option value={category}>{category}</option>
                                                            );
                                                        })}
                                                    </FormControl>
                                                </FormGroup>
                                        </ListGroupItem>
                                    );
                                })}
                            </ListGroup>
                        </Panel.Body>
                </Panel>
            
            )
        }):null;
        return (
            <div>
                <Form inline>
                    <FormGroup controlId="categoryName">
                        <ControlLabel>Category Name</ControlLabel>{' '}
                        <FormControl 
                            type="text" 
                            autoFocus
                            value={this.state.categoryName}
                            onChange={this.handleChange} 
                            placeholder="Add a new category" />
                    </FormGroup>{' '}
                    <Button 
                        onClick={this.handleNewCategory}
                        loading={this.state.categoryAdding}
                        spinColor="#000000">Add</Button>  
                </Form>
                {gifRender}
            </div>
        );
    }
};
export default withRouter(SavedGifs);