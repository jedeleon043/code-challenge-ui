
export const GIPHY = 'GIPHY';

export const storeGiphyResponse = (giphyData) => async dispatch => {
  return dispatch ({
    type: GIPHY,
    payload: { giphyData }
  });
};