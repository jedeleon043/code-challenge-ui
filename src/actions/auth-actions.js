
export const AUTHENTICATION = 'AUTHENTICATION';
export const ACCESS_TOKEN = 'ACCESS_TOKEN';

export const updateAuthentication = (isAuthenticated) => async dispatch => {
  return dispatch ({
    type: AUTHENTICATION,
    payload: { isAuthenticated }
  });
};

export const saveAccessToken = (accessToken) => async dispatch => {
  return dispatch ({
    type: ACCESS_TOKEN,
    payload: { accessToken }
  });
};