import React, { Component } from 'react';
// import logo from './logo.svg';
// import './App.css';
import HeaderContainer from './containers/HeaderContainer';
import Routes from './components/Routes'

export default class App extends Component {
  render() {
    return (
      <div id="app">
        <HeaderContainer />
        <Routes />
      </div>
    );
  }
}
